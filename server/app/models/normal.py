import re
import random
import hashlib
from app.models import database

def genkey(req):
    try:
        os = req['os']
        ver = req['ver']
        ip = req['ips']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_unregisted_servers()")
        sql_ips = cursor.fetchall()

        ips = [ip[i] for i in ip]
        to_ip = [i for i in sql_ips if ips.count(i[1]) > 0]

        if to_ip:
            server = to_ip[0]
            value = random.randint(0, 99999999999)
            client_data = os+ver+server[1]+str(value)
            hash_value = hashlib.sha512(client_data.encode("utf-8")).hexdigest()

            cursor.execute("SELECT * FROM add_server_values(%s, %s)", [server[0], hash_value])
            id = cursor.fetchone()[0]
            conn.commit()

            cursor.close()
            conn.close()

            return { "server_id": server[0], "id": id, "key": hash_value }
        else:
            cursor.execute("SELECT * FROM get_registed_servers()")
            sql_ips = cursor.fetchall()

            ips = [ip[i] for i in ip]
            to_ip = [i for i in sql_ips if ips.count(i[1]) > 0]

            cursor.execute("SELECT * FROM get_server_values(%s)", [to_ip[0][1]])
            values = cursor.fetchone()

            cursor.close()
            conn.close()
            return { "server_id": values[0], "id": values[2], "key": values[3] }
    except:
        return -1
