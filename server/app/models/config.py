import re
from app.models import database

"""
" @brief get the config from database
" @param server_id the server id
" @return -1 if fail or config and update_time from database
"""
def get_config(server_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_config_server(%s)", [server_id])
        config = cursor.fetchone()

        cursor.execute("SELECT * FROM get_server(%s)", [server_id])
        update = cursor.fetchone()[3]

        cursor.close()
        conn.close()

        return { "config": config, "update": update }
    except:
        return -1
