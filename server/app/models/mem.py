import re
from app.models import database

"""
" @brief add the mem sys to database
" @param req the response
" @param server_id the server id
" @return -1 if fail or id
"""
def add_mem_sys(req, server_id):
    try:
        perc = req['sys']['perc']
        total = req['sys']['total']
        available = req['sys']['available']
        used = req['sys']['used']
        free = req['sys']['free']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT add_mem_sys(%s, %s, %s, %s, %s, %s)", [server_id, perc, total, available, free, used])
        id = cursor.fetchone()[0]
        conn.commit()

        cursor.close()
        conn.close()

        return id
    except:
        return -1

"""
" @brief add the mem swap to database
" @param req the response
" @param server_id the server id
" @return -1 if fail or id
"""
def add_mem_swap(req, server_id):
    try:
        perc = req['swap']['perc']
        total = req['swap']['total']
        used = req['swap']['used']
        free = req['swap']['free']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT add_mem_swap(%s, %s, %s, %s, %s)", [server_id, perc, total, free, used])
        id = cursor.fetchone()[0]
        conn.commit()

        cursor.close()
        conn.close()

        return id
    except:
        return -1
