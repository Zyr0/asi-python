import re
from app.models import database

"""
" @brief get the cpu from database
" @param req the response
" @param server_id the server id
" @return -1 if fail or
"""
def get_cpu(req, server_id):
    try:
        cores = req[0]['cores']
        physical = cores['physical']
        logical = cores['logical']
        freq = req[1]['cpu_freq']
        freq_max = freq['max']
        freq_min = freq['min']
        freq_curr = freq['current']
        load_av = req[2]['load_average']
        cpu_per = req[3]['cpu_percent']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT add_cpu(%s, %s, %s, %s, %s, %s, %s, %s)", [server_id, physical, logical, freq_max, freq_min, freq_curr, load_av, cpu_per])
        c_id = cursor.fetchone()[0]
        conn.commit()

        cursor.close()
        conn.close()

        return c_id
    except:
        return -1
