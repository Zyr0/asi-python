import psycopg2

"""
" @brief create connection to database
" @return the connection to the database
"""
def get_connection():
# Connect to postgreSQL database
    return psycopg2.connect(
            host='localhost',
            database='asi_tp',
            user='asi',
            password='asi2020',
            port=5432
            )
