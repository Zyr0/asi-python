import re
from app.models import database

"""
" @brief get servers from database
" @return -1 if error from psql or the information if there is no errors
"""
def get_servers():
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM get_servers()")
        servers = cursor.fetchall()
        cursor.close()
        conn.close()
        return servers
    except:
        return -1

"""
" @brief get all server info
" @param server_id the server id
" @return server info
"""
def get_all_server_info(server_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM get_server(%s)", [server_id])
        server = list(cursor.fetchone())
        cursor.execute("SELECT * FROM get_config_server(%s)", [server_id])
        config = cursor.fetchone()

        cpus = []
        mem_sys = []
        mem_swap = []
        disks = []
        disk_values = []
        net = []
        net_values = []
        if config[1]:
            cursor.execute("SELECT * FROM get_cpu_server(%s)", [server_id])
            cpus = cursor.fetchall()
        if config[2]:
            cursor.execute("SELECT * FROM get_disk_server(%s)", [server_id])
            disks = cursor.fetchall()
            for i in disks:
                cursor.execute("SELECT * FROM get_disk_values(%s)", [i[0]])
                disk_v = cursor.fetchall()
                disk_values.append((i[0], i[1], disk_v))
        if config[3]:
            cursor.execute("SELECT * FROM get_mem_sys_server(%s)", [server_id])
            mem_sys = cursor.fetchall()
        if config[4]:
            cursor.execute("SELECT * FROM get_mem_swap_server(%s)", [server_id])
            mem_swap = cursor.fetchall()
        if config[5]:
            cursor.execute("SELECT * FROM get_net_server(%s)", [server_id])
            net = cursor.fetchall()
            for i in net:
                cursor.execute("SELECT * FROM get_net_values(%s)", [server_id])
                net_v = cursor.fetchall()
                net_values.append((i[0], i[1], net_v))

        cursor.close()
        conn.close()
        return { "server": server, "config": config, "cpus": cpus, "mem_sys": mem_sys, "mem_swap": mem_swap, "disk_values": disk_values, "net_values": net_values}
    except:
        return -1

"""
" @brief get info from a server
" @param id the server id
" @return server info, disk, net, and config
"""
def get_server_info(id):
    conn = database.get_connection()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM get_server(%s)", [id]);
    server = list(cursor.fetchone())

    cursor.execute("SELECT * FROM get_disk_server(%s)", [id]);
    disks = []
    for i in cursor:
        disks.append(i[1])

    cursor.execute("SELECT * FROM get_config_server(%s)", [id]);
    config = list(cursor.fetchone())
    config = { "cpu": config[1], "mem_sys": config[3], "mem_swap": config[4]}

    cursor.execute("SELECT * FROM get_net_server(%s)", [id]);
    net = []
    for i in cursor:
        net.append(str(i[1]))

    cursor.close()
    conn.close()
    return server, disks, net, config

"""
" @brief delete server fom database
" @param id the server id
" @return 1 if success else -1
"""
def delete_server(id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT delete_server(%s)", [id])
        conn.commit()
        cursor.close()
        conn.close()
        return 1
    except:
        return -1

"""
" @brief edit a server in the database
" @param req the request
" @return the server id if sucefful and error if not
"""
def create_server(req):
    valid = validate_server(req)
    if valid['status'] == 1:
        try:
            conn = database.get_connection()
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM add_server(%s, %s, %s)", [valid['ip'], valid['name'], valid['update_time']])
            conn.commit()
            server_id = cursor.fetchone()[0]

            cursor.execute("SELECT add_config(%s, %s, %s, %s, %s, %s)", [server_id, valid['cpu'], valid['disks'], valid['mem_sys'], valid['mem_swap'], valid['net']])
            conn.commit()
            if valid['disks'] == "TRUE":
                for i in valid['mountpoints']:
                    cursor.execute("SELECT add_disk(%s, %s)", [server_id, i])
                    conn.commit()
            if valid['net'] == "TRUE":
                for i in valid['ports']:
                    cursor.execute("SELECT add_net(%s, %s)", [server_id, i])
                    conn.commit()

            cursor.close()
            conn.close()
            return { "status": 1, "server_id": server_id }
        except:
            return { "status": -1, "message": f"Error adding information to database", "error_code": 500 }
    return valid

"""
" @brief edit a server in the database
" @param req the request
" @param id the id of the server
" @return the server id if sucefful and error if not
"""
def edit_server(req, id):
    valid = validate_server(req)
    if valid['status'] == 1:
        try:
            conn = database.get_connection()
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM edit_server(%s, %s, %s, %s)", [id, valid['ip'], valid['name'], valid['update_time']])
            server_id = cursor.fetchone()[0]
            conn.commit()

            cursor.execute("SELECT edit_config(%s, %s, %s, %s, %s, %s)", [server_id, valid['cpu'], valid['disks'], valid['mem_sys'], valid['mem_swap'], valid['net']])
            conn.commit()

            cursor.execute("SELECT * FROM get_disk_server(%s)", [server_id])
            sql_disks = [c[1] for c in cursor.fetchall()]
            add_disks = [x for x in valid['mountpoints'] if x not in sql_disks]
            remove_disks = [x for x in sql_disks if x not in valid['mountpoints']]

            for i in add_disks:
                cursor.execute("SELECT add_disk(%s, %s)", [server_id, i])
                conn.commit()

            for i in remove_disks:
                cursor.execute("SELECT disable_disk(%s, %s)", [server_id, i])
                conn.commit()

            cursor.execute("SELECT * FROM get_net_server(%s)", [server_id])
            ports = [int(x) for x in valid['ports']]
            sql_net = [c[1] for c in cursor.fetchall()]
            add_net = [x for x in ports if x not in sql_net]
            remove_net = [x for x in sql_net if x not in ports]

            for i in add_net:
                cursor.execute("SELECT add_net(%s, %s)", [server_id, i])
                conn.commit()

            for i in remove_net:
                cursor.execute("SELECT disable_net(%s, %s)", [server_id, i])
                conn.commit()

            cursor.close()
            conn.close()
            return { "status": 1, "server_id": server_id }
        except:
            return { "status": -1, "message": f"Error editing information in database", "error_code": 500 }
    return valid

"""
" @brief validate a server from a (post, put) request
" @param req
" @return a dict with the status and the message
"""
def validate_server(req):
    feedback = ""
    missing = []
    name = ""
    ip = ""
    update_time = ""
    mountpoints = []
    ports = []
    disks = "FALSE"
    net = "FALSE"
    cpu = "FALSE"
    mem_sys = "FALSE"
    mem_swap = "FALSE"

    for k, v in req.items():
        if v == "":
            missing.append(k)
        if k == 'name':
            name = v
        elif k == 'ip':
            ip = v
            feedback += validate_ip(ip)
        elif k == 'update_time':
            update_time = v
            feedback += validate_update_time(update_time)
        elif v != "" and k == 'mountpoints':
            mountpoints = v.split(' ')
            disks = "TRUE"
            feedback += validate_mountpoint(mountpoints)
        elif v != "" and k == 'ports':
            ports = v.split(' ')
            net = "TRUE"
            feedback += validate_ports(ports)
        elif k == 'cpu' and v == "true":
            cpu = "TRUE"
        elif k == 'mem_sys' and v == "true":
            mem_sys = "TRUE"
        elif k == 'mem_swap' and v == "true":
            mem_swap = "TRUE"

    if missing:
        if missing.count("name") or missing.count("ip"):
            return { "status": -1, "message": f"Missing fields for {', '.join(missing)}", "error_code": 406 }

    if feedback != "":
        return { "status": -1, "message": feedback, "error_code": 406 }

    return { "status": 1, "name": name, "ip": ip, "update_time": update_time, "mountpoints": mountpoints, "ports": ports, "disks": disks, "net": net, "cpu": cpu, "mem_sys": mem_sys, "mem_swap": mem_swap }

"""
" @brief validate a ip
" @return a string if invalid
"""
def validate_ip(ip):
    ipv = re.match("^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    if not bool(ipv) or not all(map(lambda i: 0 > int(i) or 255 > int(i), ipv.groups())):
        return f"Invalid ip {ip}, "
    return ""

"""
" @brief validate the mountpoints
" @return a string if invalid
"""
def validate_mountpoint(mountpoints):
    for i in mountpoints:
        mountv = re.match("^/[\w]", i)
        mount_v = re.match("^/", i)
        if not bool(mountv) and not bool(mount_v):
            return f"Invalid mountpoint {i}, "
    return ""

"""
" @brief validate the ports
" @return a string if invalid
"""
def validate_ports(ports):
    for i in ports:
        portsv = re.match("\d{2,4}", i)
        if not bool(portsv):
            return f"Invalid port {i}, "
    return ""

"""
" @brief validate the update time
" @return a string if invalid
"""
def validate_update_time(update_time):
    up_timev = re.match("^[0-9]{1,6}$", update_time)
    if not bool(up_timev):
        return f"Invalid update_time {update_time}, "
    return ""
