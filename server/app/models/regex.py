import re
from app.models import database

"""
" @brief get regexs from the database
" @return -1 if fail or
"""
def get_regexs():
    try:
        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_regexs()")
        regexs = cursor.fetchall()

        cursor.close()
        conn.close()
        return regexs
    except:
        return -1

"""
" @brief get regexs from the server from the database
" @param server_id the server id
" @return -1 if fail or
"""
def get_regex_server(server_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_regex_server(%s)", [server_id])
        regexs = cursor.fetchall()

        cursor.close()
        conn.close()
        return regexs
    except:
        return -1

"""
" @brief get regexs from the server from the database
" @param server_id the server id
" @return -1 if fail or
"""
def get_regex_server_values(server_id):
    try:
        regex = get_regex_server(server_id)

        conn = database.get_connection()
        cursor = conn.cursor()
        values = []

        for i in regex:
            cursor.execute("SELECT * FROM get_regex_values(%s)", [i[0]])
            values.append((i, cursor.fetchall()))

        cursor.close()
        conn.close()
        return values
    except:
        return -1

"""
" @brief get regex from the database
" @param regex_id the regex id
" @return -1 if fail or
"""
def get_regex(regex_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_regex(%s)", [regex_id])
        regex = cursor.fetchone()

        cursor.close()
        conn.close()
        return regex
    except:
        return -1

"""
" @brief delete regex from database
" @param regex_id the regex id
" @return 1 if succefull or -1
"""
def delete_regex(regex_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT delete_regex(%s)", [regex_id])
        conn.commit()
        cursor.close()
        conn.close()
        return 1
    except:
        return -1

"""
" @brief add regex values to the database
" @param req
" @param values the values of the regex
" @return -1 if fail or
"""
def add_regex_value(req, server_id):
    try:
        regex_id = req['regex_id']
        values = req['result']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT add_regex_values(%s, %s)", [regex_id, values])
        conn.commit()

        cursor.close()
        conn.close()
        return 1
    except:
        return -1

"""
" @brief get regex to the database
" @param req
" @return -1 if fail or
"""
def create_regex(req):
    valid = validate_regex(req)
    if valid['status'] != -1:
        try:
            conn = database.get_connection()
            cursor = conn.cursor()

            cursor.execute("SELECT add_regex(%s, %s, %s, %s, %s)", [valid['server_id'], valid['name'], valid['type'], valid['file'], valid['value']])
            conn.commit()

            cursor.close()
            conn.close()

            return { "status": 1, "message": f"Added regex with suckess" }
        except Exception as e:
            print(e)
            return { "status": -1, "message": f"HERE Error adding information to database", "error_code": 500 }
    return valid

"""
" @brief edit regex in the database
" @param regex_id the regex id
" @param req
" @return -1 if fail or
"""
def edit_regex(req, regex_id):
    valid = validate_regex(req)
    if valid['status'] != -1:
        try:
            conn = database.get_connection()
            cursor = conn.cursor()

            cursor.execute("SELECT edit_regex(%s, %s, %s, %s, %s, %s)", [regex_id, valid['server_id'], valid['name'], valid['type'], valid['file'], valid['value']])
            conn.commit()

            cursor.close()
            conn.close()
            return { "status": 1, "message": f"Edited regex with suckess" }
        except:
            return { "status": -1, "message": f"Error adding information to database", "error_code": 500 }
    return valid

"""
" @brief validate regex
" @param req
" @return status -1 if failed
"""
def validate_regex(req):
    feedback = ""
    missing = []
    name = ""
    value = ""
    server_id = ""
    file = ""
    type = ""

    for k, v in req.items():
        if v == "":
            missing.append(k)
        elif k == 'type':
            type = v
            feedback += validate_type(type)
        elif k == 'name':
            name = v
        elif k == 'value':
            value = v
            feedback += validate_value(value)
        elif k == 'server_id':
            server_id = v
            feedback += validate_server_id(server_id)
        elif v != "" and k == 'files':
            file = v
            feedback += validate_file(file)

    if missing:
        return { "status": -1, "message": f"Missing fields for {', '.join(missing)}", "error_code": 406 }

    if feedback != "":
        return { "status": -1, "message": feedback, "error_code": 406 }

    return { "status": 1, "name": name, "type": type, "file": file, "server_id": server_id, "value": value }

"""
" @brief validate the regex type
" @param value the type
" @return a string if invalid
"""
def validate_type(value):
    if value == 'match' or value == 'all' or value == 'search':
        return ""
    return f"Invalid type {value}, "

"""
" @brief validate the regex value
" @param value the regex
" @return a string if invalid
"""
def validate_value(value):
    try:
        re.compile(value)
        return ""
    except re.error:
        return f"Invalid regex {value}, "

"""
" @brief validate the server_id
" @param server_id the server_id
" @return a string if invalid
"""
def validate_server_id(server_id):
    if type(int(server_id)) != int:
        return f"Invalid file {server_id}, "
    return ""

"""
" @brief validate the file path
" @param file the file name
" @return a string if invalid
"""
def validate_file(file):
    filev = re.match("^(/[a-zA-Z0-9_\.-]+)+$", file)
    if not bool(filev):
        return f"Invalid file {file}, "
    return ""
