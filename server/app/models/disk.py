import re
from app.models import database

"""
" @brief get the disks from database
" @param server_id the server id
" @return -1 if fail or disks
"""
def get_disk(server_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM get_disk_server(%s)", [server_id])
        disks = []
        for i in cursor:
            disks.append(i[1])

        cursor.close()
        conn.close()
        return disks
    except:
        return -1

"""
" @brief add new disk_value to database
" @param req the request
" @param server_id the server id
" @return -1 if error or id
"""
def add_disk(req, server_id):
    try:
        mountpoint = req['mountpoint']
        total = req['total']
        used = req['used']
        free = req['free']
        inodes = req['inodes']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_disk(%s, %s)", [server_id, mountpoint])
        disk = cursor.fetchone()

        if disk[3] == False:
            cursor.execute("SELECT add_disk_values(%s, %s, %s, %s, %s)", [disk[0], total, free, used, inodes])
            id = cursor.fetchone()[0]
            conn.commit()
            cursor.close()
            conn.close()
            return id
        cursor.close()
        conn.close()
        return -1
    except:
        return -1
