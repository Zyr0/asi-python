import re
from app.models import database

"""
" @brief get the net from database
" @param server_id the server id
" @return -1 if fail or disks
"""
def get_net(server_id):
    try:
        conn = database.get_connection()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM get_net_server(%s)", [server_id])
        net = []
        for i in cursor:
            net.append(i[1])

        cursor.close()
        conn.close()
        return net
    except:
        return -1

"""
" @brief add new net to database
" @param req the request
" @param server_id the server id
" @return -1 if error or id
"""
def add_net(req, server_id):
    try:
        port = req['port']
        status = req['status']

        conn = database.get_connection()
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM get_net(%s, %s)", [server_id, port])
        net = cursor.fetchone()

        if net[3] == False:
            cursor.execute("SELECT add_net_value(%s, %s)", [net[0], status])
            id = cursor.fetchone()[0]
            conn.commit()
            cursor.close()
            return id

        cursor.close()
        conn.close()
        return -1
    except:
        return -1
