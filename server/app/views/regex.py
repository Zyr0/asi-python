from flask import render_template, request, redirect, url_for, jsonify
from app import app
from app.models import InvalidUsage as ex, regex as r, server as s

@app.route('/regex/', methods=['GET'])
def regex():
    servers = s.get_servers()
    regexs = r.get_regexs()
    if regexs != -1:
        return render_template('regex/index.html', regexs=regexs, n_regexs=len(regexs), n_servers=len(servers))
    raise ex.InvalidUsage("Error getting information from database", 500)

@app.route('/regex/<server_id>', methods=['GET'])
def get_regex(server_id):
    regex = r.get_regex_server(server_id)
    if regex != -1:
        return jsonify(regex)
    raise ex.InvalidUsage("Error getting information from database", 500)

@app.route('/regex/<server_id>', methods=['POST'])
def post_get_regex(server_id):
    try:
        req = request.get_json()
        regex = r.add_regex_value(req, server_id)
        if regex != -1:
            return jsonify(regex)
        raise ex.InvalidUsage("Error getting information from database", 500)
    except:
        raise ex.InvalidUsage("Error getting information", 400)
@app.route('/regex/add', methods=['GET'])
def add_regex():
    servers = s.get_servers()
    if servers != -1:
        return render_template('regex/add_regex.html', servers=servers, n_servers=len(servers))
    raise ex.InvalidUsage("Error getting information from database", 500)

@app.route('/regex/edit/<regex_id>', methods=['GET'])
def edit_regex(regex_id):
    servers = s.get_servers()
    regex = r.get_regex(regex_id)
    if servers != -1:
        return render_template('regex/edit_regex.html', servers=servers, regex=regex)
    raise ex.InvalidUsage("Error getting information from database", 500)

@app.route('/regex/edit/<regex_id>', methods=['POST'])
def post_edit_regex(regex_id):
    req = request.form
    valid = r.edit_regex(req, regex_id)
    if valid['status'] != -1:
        return jsonify({ "message": valid['message'] })
    raise ex.InvalidUsage(valid['message'], valid['error_code'])

@app.route('/regex/add', methods=['POST'])
def post_add_server():
    req = request.form
    valid = r.create_regex(req)
    if valid['status'] != -1:
        return jsonify({ "message": valid['message'] })
    raise ex.InvalidUsage(valid['message'], valid['error_code'])

@app.route('/regex/delete/<regex_id>', methods=['DELETE'])
def delete_regex(regex_id):
    info = r.delete_regex(regex_id)
    if info == 1:
        return jsonify({ "message": "Server Deleted" })
    raise ex.InvalidUsage("Error deleting regex from database", 500)
