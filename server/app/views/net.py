from flask import request, jsonify
from app import app
from app.models import InvalidUsage as ex, net as n

@app.route('/net/<server_id>', methods=['POST'])
def add_net_values(server_id):
    try:
        req = request.get_json()
        values = n.add_net(req, server_id)
        if values != -1:
            return jsonify(values)
        raise ex.InvalidUsage("Network is disabled", 401)
    except:
        raise ex.InvalidUsage("Error adding network values", 400)

@app.route('/net/<server_id>', methods=['GET'])
def get_net(server_id):
    try:
        values = n.get_net(server_id)
        if values != -1:
            return jsonify(values)
        raise ex.InvalidUsage("Error getting disk values", 500)
    except:
        raise ex.InvalidUsage("Error getting disk values", 400)
