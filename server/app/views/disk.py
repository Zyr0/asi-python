from flask import request, jsonify, abort
from app import app
from app.models import InvalidUsage as ex, disk as d

@app.route('/disk/<server_id>', methods=['POST'])
def add_disk_values(server_id):
    try:
        req = request.get_json()
        disk = d.add_disk(req, server_id)
        if disk != -1:
            return jsonify(disk)
    except:
        raise ex.InvalidUsage("Disk is disabled", status_code=401)

@app.route('/disk/<server_id>', methods=['GET'])
def get_disks(server_id):
    try:
        disks = d.get_disk(server_id)
        if disks != -1:
            return jsonify(disks)
        raise ex.InvalidUsage("Error getting disks values", 500)
    except:
        raise ex.InvalidUsage("Error getting disk values", 400)
