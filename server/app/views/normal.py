from flask import render_template, request, redirect, url_for, jsonify
from app import app
from app.models import InvalidUsage as ex, normal as n

@app.route('/', methods=['GET'])
def test():
    return redirect(url_for('server'))

@app.route('/genkey', methods=['POST'])
def genkey():
    req = request.get_json()
    genkey = n.genkey(req)
    if genkey != -1:
        return jsonify(genkey)
    m = f"Error generating key"
    raise ex.InvalidUsage(m, status_code=400)
