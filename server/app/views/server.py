from flask import render_template, request, redirect, url_for, jsonify
from app import app
from app.models import InvalidUsage as ex, server as model_server, regex as r

@app.route('/servers', methods=['GET'])
def server():
    info = model_server.get_servers()
    if info != -1:
        return render_template('server/index.html', servers=info, n_servers=len(info))
    raise ex.InvalidUsage("Error getting information from database", 500)

@app.route('/servers/add', methods=['POST'])
def post_server_add():
    req = request.form
    valid = model_server.create_server(req)
    if valid['status'] != -1:
        return jsonify({ "message": f"Created succefuly with id {valid['server_id']}" })
    raise ex.InvalidUsage(valid['message'], valid['error_code'])

@app.route('/servers/add', methods=['GET'])
def server_add():
    return render_template('server/add_server.html')

@app.route('/servers/edit/<id>', methods=['POST'])
def post_server_edit(id):
    req = request.form
    valid = model_server.edit_server(req, id)
    if valid['status'] != -1:
        return jsonify({ "message": f"Edited succefuly with id {valid['server_id']}" })
    raise ex.InvalidUsage(valid['message'], valid['error_code'])

@app.route('/servers/edit/<id>', methods=['GET'])
def server_edit(id):
    values = model_server.get_server_info(id)
    return render_template('server/edit_server.html', server=values[0], disks=' '.join(values[1]), net=' '.join(values[2]), config=values[3])

@app.route('/servers/delete/<server_id>', methods=['DELETE'])
def server_delete(server_id):
    info = model_server.delete_server(server_id)
    if info == 1:
        return jsonify({ "message": "Server Deleted" })
    raise ex.InvalidUsage("Error deleting server from database", 500)

@app.route('/servers/info/<server_id>', methods=['GET'])
def server_info(server_id):
    server = model_server.get_all_server_info(server_id)
    regex = r.get_regex_server_values(server_id)
    if server != -1 and regex != -1:
        return render_template('server/info_server.html', server=server['server'], config=server['config'],
                cpus=server['cpus'], disk_values=server['disk_values'], mem_sys=server['mem_sys'],
                mem_swap=server['mem_swap'], net_values=server['net_values'], regex=regex)
    raise ex.InvalidUsage("Error getting server from database", 500)
