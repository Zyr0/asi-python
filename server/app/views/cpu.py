from flask import request, jsonify
from app import app
from app.models import InvalidUsage as ex, cpu as c

@app.route('/cpu/<server_id>', methods=['POST'])
def add_cpu_values(server_id):
    try:
        req = request.get_json()['cpu']
        values = c.get_cpu(req, server_id)
        if values != -1:
            return jsonify({ "id": values })
        raise ex.InvalidUsage("Error adding values", 500)
    except Exception as e:
        m = f"Error adding cpu values"
        raise ex.InvalidUsage(m, status_code=400)
