from flask import request, jsonify
from app import app
from app.models import InvalidUsage as ex, mem as m

@app.route('/mem_sys/<server_id>', methods=['POST'])
def add_mem_sys_values(server_id):
    try:
        req = request.get_json()
        value = m.add_mem_sys(req, server_id)
        if value != -1:
            return jsonify(value)
        raise ex.InvalidUsage("Error adding mem sys values" , 500)
    except Exception as e:
        raise ex.InvalidUsage("Error adding mem sys values" , status_code=400)


@app.route('/mem_swap/<server_id>', methods=['POST'])
def add_mem_swap_values(server_id):
    try:
        req = request.get_json()
        value = m.add_mem_swap(req, server_id)
        if value != -1:
            return jsonify(value)
        raise ex.InvalidUsage("Error adding mem sys values" , 500)
    except Exception as e:
        raise ex.InvalidUsage("Error adding mem sys values" , status_code=400)
