from flask import jsonify
from app import app
from app.models import InvalidUsage as ex

@app.errorhandler(ex.InvalidUsage)
def handle_invalid_usage(error):
   response = jsonify(error.to_dict())
   response.status_code = error.status_code
   return response
