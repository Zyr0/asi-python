from flask import request, jsonify
from app import app
from app.models import InvalidUsage as ex, config as c

@app.route('/config/<server_id>', methods=['GET'])
def get_config_server(server_id):
    try:
        values = c.get_config(server_id)
        if values != -1:
            config = values['config']
            update = values['update']
            return jsonify({ 'update_time': update, 'cpu': config[1], 'disk': config[2], 'mem_sys': config[3], 'mem_swap': config[4], 'net': config[5]})
        raise ex.InvalidUsage("Error getting values from database", 500)
    except:
        m = f"Error getting config"
        raise ex.InvalidUsage(m, status_code=500)
