from flask import Flask

app = Flask(__name__)

from app.views import error
from app.views import normal
from app.views import server
from app.views import cpu
from app.views import mem
from app.views import disk
from app.views import net
from app.views import config
from app.views import regex
