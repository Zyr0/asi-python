
"""
" Configs
"""
HTTPS=False
HOST='localhost'
PORT=5000
CEIL=True

"""
" Files
"""
KEY_FILE=".key"

"""
" Error Messages
"""
TIMEOUT_ERR = "Connection Timeout: "
CONNECTION_ERR = "Error Connecting: "
HTTP_ERR = "HTTP ERROR: "
REQUEST_ERR = "OOps: Something Else: "
FILE_ERR = "Could Not Open/Read File: "

"""
" @brief build an url with the HTTPS, HOST, PORT information
" @param the path affter the url ex: https://exe.pt/<path>
" @return the complete url
"""
def get_url(path=""):
    h = "https://" if HTTPS else "http://"
    return h + HOST + ":" + str(PORT) + "/" + path

