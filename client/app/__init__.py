import requests
import time
import json

from app import config
from app.config import get_url
from app.modules import cpu, mem, disk, net, sys, utils, regex
import app.modules.request_handler as r_handle

"""
" @brief get client key, if key doen't exist in KEY_FILE
" @throws Execption if failed to comminicate with the
" server or faild to store the key
"""
def getkey():
    if not utils.exists_file(config.KEY_FILE):
        res = r_handle.handle_post_requests(get_url("genkey"), sys.get_inital_information())
        if res.ok:
            with open(config.KEY_FILE, "a") as fp:
                json.dump(res.json(), fp)
        else:
            raise Exception(res.json()['error_message'])

"""
" @brief read KEY_FILE to get server id
" @return the server id
"""
def get_server_from_key():
    server_id = -1;
    with open(config.KEY_FILE, "r") as fp:
        server_id = json.load(fp)['server_id']
    return server_id

"""
" @brief get the config from the server
" @returns the config
" @throws Exception with error message if comunication fails
"""
def get_config_values(server_id):
    res = r_handle.handle_get_requests(get_url(f"config/{ server_id }"))
    if res.ok:
        return res.json()
    else:
        raise Exception(res.json()['error_message'])

"""
" @brief get the mountpoinst from the server
" @returns the mountpoints
" @throws Exception with error message if comunication fails
"""
def get_mountpoinst(server_id):
    res = r_handle.handle_get_requests(get_url(f"disk/{ server_id }"))
    if res.ok:
        return res.json()
    else:
        raise Exception(res.json()['error_message'])

"""
" @brief get the ports from the server
" @returns the ports
" @throws Exception with error message if comunication fails
"""
def get_net(server_id):
    res = r_handle.handle_get_requests(get_url(f"net/{ server_id }"))
    if res.ok:
        return res.json()
    else:
        raise Exception(res.json()['error_message'])

"""
" @brief get the regex from the server
" @returns the regex
" @throws Exception with error message if comunication fails
"""
def get_regex(server_id):
    res = r_handle.handle_get_requests(get_url(f"regex/{ server_id }"))
    if res.ok:
        return res.json()
    else:
        raise Exception(res.json()['error_message'])

"""
" @brief run the program in a loop
"""
def run():
    while (True):
        try:
            getkey()

            server_id = get_server_from_key()
            config_v = get_config_values(server_id)
            regex_v = get_regex(server_id)

            if config_v['cpu']:
                res = r_handle.handle_post_requests(get_url(f"cpu/{ server_id }"), cpu.get_all(ceil=config.CEIL))
                if not res.ok:
                    raise Exception(res.json()['error_message'])
            if config_v['disk']:
                for i in get_mountpoinst(server_id):
                    res = r_handle.handle_post_requests(get_url(f"disk/{ server_id }"), disk.get_all(i, ceil=config.CEIL))
                    if not res.ok:
                        raise Exception(res.json()['error_message'])
            if config_v['mem_sys']:
                res = r_handle.handle_post_requests(get_url(f"mem_sys/{ server_id }"), mem.get_sys_mem(ceil=config.CEIL))
                if not res.ok:
                    raise Exception(res.json()['error_message'])
            if config_v['mem_swap']:
                res = r_handle.handle_post_requests(get_url(f"mem_swap/{ server_id }"), mem.get_swap_mem(ceil=config.CEIL))
                if not res.ok:
                    raise Exception(res.json()['error_message'])
            if config_v['net']:
                for i in get_net(server_id):
                    res = r_handle.handle_post_requests(get_url(f"net/{ server_id }"), net.check_port(i))
                    if not res.ok:
                        raise Exception(res.json()['error_message'])
            if regex_v:
                for i in regex_v:
                    res = r_handle.handle_post_requests(get_url(f"regex/{ server_id }"), regex.get_regex(i))
                    if not res.ok:
                        raise Exception(res.json()['error_message'])

            print("Started Slepping")
            time.sleep(config_v['update_time'] * 60)
            print("End Slepping")

        except IOError:
            print(config.FILE_ERR)
        except Exception as e:
            print(e)
