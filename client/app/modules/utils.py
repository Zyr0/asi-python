"""
" @package modules
" @name utils
" @brief module for custom utils
" @author João Cunha
"""

import os

"""
" @brief convert from bytes to mib
" @param _bytes the memory in bytes
" @return _bytes value in mib
"""
def convert_to_mib(_bytes):
    return _bytes / 1024**2

"""
" @brief check if a file exists
" @return true if exists false if not
"""
def exists_file(path):
    return os.path.isfile(path)
