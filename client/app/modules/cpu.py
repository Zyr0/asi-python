"""
" @package modules
" @name cpu
" @brief this module is responsible for handling information related to the cpu this uses a third
" party lib called psutil that works for Linux, Windows, macOS, BSD, Solaris and AIX
" @author João Cunha
"""

import psutil
import math
import app.modules.utils

"""
" @brief get logcal cores of the cpu
" @return the number of logical cores
"""
def get_logical_cores():
    return psutil.cpu_count(True)

"""
" @brief get physical cores of the cpu
" @return the number of physical cores
"""
def get_physical_cores():
    return psutil.cpu_count(False)

"""
" @brief get cores of the cpu logical and physical
" @return an json { 'core': [ {'physical': <n_cores>}, {'logical': <n_cores>}]}
"""
def get_cores():
    return { 'cores': { 'physical': get_physical_cores(), 'logical': get_logical_cores() }}

"""
" @brief get the cpu frequency in MHz
" @warning only availabe in Linux, macOS, Windows, FreeBSD
" @param ceil default = false
" @return a json { 'cpu_freq': { 'max': <max> }, { 'min': <min> }, { 'current': <current> } }
"""
def get_cpu_freq(ceil=False):
    freq = psutil.cpu_freq()
    value = math.ceil(freq.current) if ceil else freq.current
    return { 'cpu_freq': { 'max': freq.max, 'min': freq.min, 'current': value } }

"""
" @brief get the load average system load of the last minute
" @waring only available on Unix and Windows systems
" @return a json { 'load_average': <load in %> }
"""
def get_loadavg():
    load = psutil.getloadavg()
    cores = get_logical_cores()
    return { 'load_average': (load[0] / cores) * 100 }

"""
" @brief get cpu utilization in percentage from since last call
" @return a json { 'cpu_precent': <precent> }
"""
def get_cpu_percent():
    pre = psutil.cpu_percent(0) # whitout interval returns 0 witch means that this is a bloking thread
    return { 'cpu_percent': pre }

"""
" @brief get all cpu info using the functions in this file
" @return a json with all the cpu info
"""
def get_all(ceil=False):
    return { "cpu": [get_cores(), get_cpu_freq(ceil), get_loadavg(), get_cpu_percent()] }
