"""
" @package modules
" @name mem
" @brief this module is responsible for handling information related to the memory this uses a third
" party lib called psutil that works for Linux, Windows, macOS, BSD, Solaris and AIX
" @author João Cunha
"""

import psutil
import math
import app.modules.utils as utils

"""
" @brief get system memory in percentage
" @return precent of memory being used
"""
def get_perc_mem():
    return psutil.virtual_memory().percent

"""
" @brief get total system memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_total_mem(converted=True, ceil=False):
    mem = psutil.virtual_memory().total
    mem = utils.convert_to_mib(mem) if converted else mem
    mem = math.ceil(mem) if ceil else mem
    return mem

"""
" @brief get availabe system memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_available_mem(converted=True, ceil=False):
    mem = psutil.virtual_memory().available
    mem = utils.convert_to_mib(mem) if converted else mem
    mem = math.ceil(mem) if ceil else mem
    return mem

"""
" @brief get used system memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_used_mem(converted=True, ceil=False):
    mem = psutil.virtual_memory().used
    mem = utils.convert_to_mib(mem) if converted else mem
    mem = math.ceil(mem) if ceil else mem
    return mem

"""
" @brief get free system memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_free_mem(converted=True, ceil=False):
    mem = psutil.virtual_memory().free
    mem = utils.convert_to_mib(mem) if converted else mem
    mem = math.ceil(mem) if ceil else mem
    return mem

"""
" @brief get system swap in percentage
" @param ceil default = false
" @return precent of swap being used
"""
def get_perc_swap(converted=True, ceil=False):
    return psutil.swap_memory().percent

"""
" @brief get total swap memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_total_swap(converted=True, ceil=False):
    swap = psutil.swap_memory().total
    swap = utils.convert_to_mib(swap) if converted else swap
    swap = math.ceil(swap) if ceil else swap
    return swap

"""
" @brief get used swap memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_used_swap(converted=True, ceil=False):
    swap = psutil.swap_memory().used
    swap = utils.convert_to_mib(swap) if converted else swap
    swap = math.ceil(swap) if ceil else swap
    return swap

"""
" @brief get free swap memory in bytes or in mib
" @param converted default = true
" @param ceil default = false
" @return memory in mib if converted == true or in bytes if converted == false
"""
def get_free_swap(converted=True, ceil=False):
    swap = psutil.swap_memory().free
    swap = utils.convert_to_mib(swap) if converted else swap
    swap = math.ceil(swap) if ceil else swap
    return swap

"""
" @brief get all system memory
" @param ceil default = false
" @return a json with the sys memory values
"""
def get_sys_mem(converted=True, ceil=False):
    return { 'sys': {
        'perc': get_perc_mem(),
        'total': get_total_mem(converted, ceil),
        'available': get_available_mem(converted, ceil),
        'used': get_used_mem(converted, ceil),
        'free': get_free_mem(converted, ceil)
    }}

"""
" @brief get all swap memory
" @param ceil default = false
" @return a json with the swap memory values
"""
def get_swap_mem(converted=True, ceil=False):
    return { 'swap': {
        'perc': get_perc_swap(),
        'total': get_total_swap(converted, ceil),
        'used': get_used_swap(converted, ceil),
        'free': get_free_swap(converted, ceil)
    }}
