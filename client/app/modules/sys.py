"""
" @package modules
" @name sys
" @brief module responsible to access sys options
" @author João Cunha
"""

import platform
import app.modules.net as net

"""
" @brief get the operation system
" @return string with the operation system
"""
def get_os_platform():
    return { 'os': platform.system() }

"""
" @brief get the operation system version
" @return string with the operation system version
"""
def get_os_version():
    return { 'ver': platform.release() }

"""
" @brief get os with all information
" @return string with the operation system information
"""
def get_os_info():
    return { 'os_info': [get_os_platform(), get_os_version()] }

"""
" @brief get system information using the modules
" @return the json of the os the version and a list of ips
"""
def get_inital_information():
    os = get_os_info()['os_info']
    return { 'os': os[0]['os'], 'ver': os[1]['ver'], 'ips': net.get_ipv4()}
