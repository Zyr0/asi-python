import app.config as config
import requests

"""
" @brief post a request to a url and upload json data
" @param url the url to make the request
" @param data the data to send
" @return the response from the request
"""
def handle_post_requests(url, data):
    try:
        res = requests.post(url, json=data)
        res.raise_for_status()
        return res
    except requests.exceptions.HTTPError as http_err:
        print(config.HTTP_ERR, http_err)
        return res
    except requests.exceptions.Timeout as timeout:
        print(config.CONNECTION_ERR, timeout)
    except requests.exceptions.ConnectionError as conn_err:
        print(config.TIMEOUT_ERR, conn_err)
    except requests.exceptions.RequestException as err:
        print(config.REQUEST_ERR, err)

"""
" @brief post a request to a url and upload json data
" @param url the url to make the request
" @return the response from the request
"""
def handle_get_requests(url):
    try:
        res = requests.get(url)
        res.raise_for_status()
        return res
    except requests.exceptions.HTTPError as http_err:
        print(config.HTTP_ERR, http_err)
        return res
    except requests.exceptions.ConnectionError as conn_err:
        print(config.TIMEOUT_ERR, conn_err)
    except requests.exceptions.Timeout as timeout:
        print(config.CONNECTION_ERR, timeout)
    except requests.exceptions.RequestException as err:
        print(config.REQUEST_ERR, err)
