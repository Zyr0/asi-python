"""
" @package modules
" @name regex
" @brief this module is responsible for handling information related to the regex
" @author João Cunha
"""

import re
import app.config as config

"""
" @brief get regex from file
" @return the result
"""
def get_regex(regex):
    result = ""
    count = 0;
    try:
        if regex[2] == 'match':
            for line in open(regex[3]):
                if re.match(regex[4], line):
                    result += " -> "
                    result += line
        elif regex[2] == 'all':
            for line in open(regex[3]):
                res = re.findall(regex[4], line)
                if res:
                    count += len(res)
            result = str(count)
        elif regex[2] == 'search':
            for line in open(regex[3]):
                res = re.search(regex[4], line)
                if res:
                    count += 1
            result = str(count)
        else:
            result = "None"
    except OSError:
        result = config.FILE_ERR

    return { "regex_id": regex[0], "result": result }
