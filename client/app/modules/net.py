"""
" @package modules
" @name net
" @brief this module is responsible for handling information related to the net this uses a third
" party lib called psutil that works for Linux, Windows, macOS, BSD, Solaris and AIX
" @author João Cunha
"""

import re
import psutil

"""
" @brief get all ipv4s from the system
" @return the list of ipv4s
"""
def get_ipv4():
    table = {}
    ip = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
    for nic, addrs in psutil.net_if_addrs().items():
        for addr in addrs:
            if ip.match(addr.address):
                table[nic] = addr.address
    return table

"""
" @brief see if a port is open
" @return true if port is open false if not
"""
def is_port_open(port):
    for i in psutil.net_connections():
        if i.laddr.port == port and i.status == 'LISTEN':
            return True
    return False

"""
" @brief a method that checks if the port is open
" @return a json with the status and the port
"""
def check_port(port):
    return { 'port': port, 'status': is_port_open(port) }
