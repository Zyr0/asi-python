"""
" @package modules
" @name disk
" @brief this module is responsible for handling information related to the disk this uses a third
" party lib called psutil that works for Linux, Windows, macOS, BSD, Solaris and AIX
" @author João Cunha
"""

import psutil
import os
import math
import app.modules.utils as utils

"""
" @brief get all of the disk partitions with mountpoints
" @return json with device and mountpoint
"""
def get_disk_mountpoints():
    disks = {}
    for disk in psutil.disk_partitions():
        disks[disk.device] = disk.mountpoint
    return disks

"""
" @brief get total disk space from a mountpoint
" @param mountpoint the mountpoint
" @param ceil default == False
" @return the total space in mib
"""
def get_total_disk(mountpoint, ceil=False):
    disk = psutil.disk_usage(mountpoint)
    value = utils.convert_to_mib(disk.total)
    if ceil:
        return math.ceil(value)
    return value

"""
" @brief get used disk space from a mountpoint
" @param mountpoint the mountpoint
" @param ceil default == False
" @return the used space in mib
"""
def get_used_disk(mountpoint, ceil=False):
    total = psutil.disk_usage(mountpoint)
    value = utils.convert_to_mib(total.used)
    if ceil:
        return math.ceil(value)
    return value

"""
" @brief get free disk space from a mountpoint
" @param mountpoint the mountpoint
" @param ceil default == False
" @return the free space in mib
"""
def get_free_disk(mountpoint, ceil=False):
    disk = psutil.disk_usage(mountpoint)
    value = utils.convert_to_mib(disk.free)
    if ceil:
        return math.ceil(value)
    return value

"""
" @brief get disk inodes from a mountpoint
" @param mountpoint the mountpoint
" @param ceil default == False
" @return percentage of inodes in use
"""
def get_inodes_disk(mountpoint, ceil=False):
    disk = os.statvfs(mountpoint)
    total_inode = utils.convert_to_mib(disk.f_files)
    free_inodes = utils.convert_to_mib(disk.f_ffree)
    value = float(total_inode - free_inodes) / total_inode * 100
    if ceil:
        return math.ceil(value)
    return value

"""
" @brief get all disk information
" @param ceil default == False
" @return a json with the data
"""
def get_all(mountpoint, ceil=False):
    return { 'mountpoint': mountpoint,
             'total': get_total_disk(mountpoint, ceil),
             'used': get_used_disk(mountpoint, ceil),
             'free': get_free_disk(mountpoint, ceil),
             'inodes': get_inodes_disk(mountpoint, ceil) }
