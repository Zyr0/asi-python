#!/bin/bash

read -p "Run docker-compose [y/N]: " op
op=${op:-n}

[[ $op == "n" ]] || [[ $op == "N" ]] && exit;

echo "Starting postgres container ... "
docker-compose up -d

read -p "Run create_db script [y/N]: " op
op=${op:-n}

[[ $op == "n" ]] || [[ $op == "N" ]] && exit;

echo "Runnig sql script ... "
docker exec -it db bash ./sql/create_db.sh -d
