\c asi_tp

CREATE OR REPLACE FUNCTION get_mem_swap()
RETURNS SETOF RECORD LANGUAGE SQL
AS $$
  SELECT * FROM mem_swap;
$$;

CREATE OR REPLACE FUNCTION get_mem_swap(
  _id INT
)
RETURNS RECORD LANGUAGE SQL
AS $$
  SELECT * FROM mem_swap WHERE id = _id;
$$;

CREATE OR REPLACE FUNCTION get_mem_swap_server(
  _server_id INT
)
RETURNS TABLE (
  id INT,
  server_id INT,
  perc NUMERIC,
  total NUMERIC,
  used NUMERIC,
  free NUMERIC,
  add_date TIMESTAMP) AS $$
  BEGIN
    RETURN QUERY SELECT ms.id, ms.server_id, ms.perc, ms.total, ms.used, ms.free, ms.add_date FROM mem_swap ms WHERE ms.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_mem_swap(
  _server_id INT,
  _perc NUMERIC,
  _total NUMERIC,
  _free NUMERIC,
  _used NUMERIC
)
RETURNS TABLE (id int) LANGUAGE SQL
AS $$
  INSERT INTO mem_swap (server_id, perc, total, free, used) VALUES (_server_id, _perc, _total, _free, _used);
  SELECT id FROM mem_swap WHERE server_id = _server_id ORDER BY id DESC;
$$;

CREATE OR REPLACE FUNCTION edit_mem_swap(
  _id INT,
  _server_id INT,
  _perc NUMERIC,
  _total NUMERIC,
  _free NUMERIC,
  _used NUMERIC
)
RETURNS RECORD LANGUAGE SQL
AS $$
  UPDATE mem_swap SET server_id = _server_id, perc = _perc, total = _total, free = _free, used = _used, edit_date = CURRENT_TIMESTAMP WHERE id = _id;
  SELECT * FROM mem_swap WHERE server_id = _server_id;
$$;
