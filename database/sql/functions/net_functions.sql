\c asi_tp

CREATE OR REPLACE FUNCTION get_nets()
RETURNS SETOF RECORD AS $$
  BEGIN
    RETURN QUERY SELECT * FROM net;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_net(
  _server_id INT,
  _port INT
)
RETURNS TABLE (id INT, server_id INT, port INT, disabled BOOLEAN) AS $$
  BEGIN
    RETURN QUERY SELECT n.id, n.server_id, n.port, n.disabled FROM net n WHERE n.server_id = _server_id AND n.port = _port;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_net_server(
  _server_id INT
)
RETURNS TABLE (id INT, port INT) AS $$
  BEGIN
    RETURN QUERY SELECT n.id, n.port FROM net n WHERE n.server_id = _server_id AND n.disabled = FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_net_values(
  _server_id INT
)
RETURNS TABLE (id INT, port INT, is_open BOOLEAN, add_date TIMESTAMP) AS $$
  BEGIN
    RETURN QUERY SELECT n.id, n.port, nv.is_open, nv.add_date FROM net n INNER JOIN net_values nv ON nv.net_id = n.id WHERE n.server_id = _server_id AND n.disabled = FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_net(
  _server_id INT,
  _port INT
)
RETURNS TABLE (id int) AS $$
  DECLARE
    _val INT;
  BEGIN
    SELECT n.id FROM net n INTO _val WHERE n.server_id = _server_id AND n.port = _port;
    IF _val > 0 THEN
      UPDATE net n SET disabled = FALSE WHERE n.server_id = _server_id AND n.port = _port;
    ELSE
      INSERT INTO net (server_id, port, disabled) VALUES (_server_id, _port, FALSE);
    END IF;
    RETURN QUERY SELECT n.id FROM net n WHERE n.server_id = _server_id AND n.port = _port;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_net_value(
  _net_id INT,
  _is_open BOOLEAN
)
RETURNS TABLE (id int) AS $$
  BEGIN
    INSERT INTO net_values (net_id, is_open) VALUES (_net_id, _is_open);
    RETURN QUERY SELECT nv.id FROM net_values nv WHERE nv.net_id = _net_id ORDER BY nv.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION edit_net(
  _id INT,
  _server_id INT,
  _port INT
)
RETURNS TABLE (id INT) AS $$
  BEGIN
    UPDATE net n SET server_id = _server_id, port = _port, edit_date = CURRENT_TIMESTAMP WHERE n.id = _id;
    RETURN QUERY SELECT n.id FROM net n WHERE n.id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disable_net(
  _server_id INT,
  _port INT
)
RETURNS TABLE (id INT) AS $$
  BEGIN
    UPDATE net n SET disabled = TRUE WHERE n.server_id = _server_id AND n.port = _port;
    RETURN QUERY SELECT n.id FROM net n WHERE n.server_id = _server_id AND n.port = _port;
  END;
$$ LANGUAGE plpgsql;
