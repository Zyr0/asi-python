\c asi_tp

CREATE OR REPLACE FUNCTION get_cpus()
RETURNS TABLE (
  id INT,
  server_id INT,
  logical INT,
  pyshical INT,
  freq_max NUMERIC,
  freq_min NUMERIC,
  freq_current NUMERIC,
  load_average NUMERIC,
  cpu_percent NUMERIC,
  add_date TIMESTAMP,
  edit_date TIMESTAMP ) AS $$
  BEGIN
    RETURN QUERY SELECT c.* FROM cpu c;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_cpu(
  _id INT
)
RETURNS TABLE (
  id INT,
  server_id INT,
  logical INT,
  pyshical INT,
  freq_max NUMERIC,
  freq_min NUMERIC,
  freq_current NUMERIC,
  load_average NUMERIC,
  cpu_percent NUMERIC) AS $$
  BEGIN
    RETURN QUERY SELECT c.id, c.server_id, c.logical, c.pyshical, c.freq_max, c.freq_min, c.freq_current, c.load_average, c.cpu_percent FROM cpu c WHERE c.id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_cpu_server(
  _server_id INT
)
RETURNS TABLE (
  id INT,
  server_id INT,
  logical INT,
  pyshical INT,
  freq_max NUMERIC,
  freq_min NUMERIC,
  freq_current NUMERIC,
  load_average NUMERIC,
  cpu_percent NUMERIC,
  add_date TIMESTAMP) AS $$
  BEGIN
    RETURN QUERY SELECT c.id, c.server_id, c.logical, c.pyshical, c.freq_max, c.freq_min, c.freq_current, c.load_average, c.cpu_percent, c.add_date FROM cpu c WHERE c.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_cpu(
  _server_id INT,
  _logical INT,
  _pyshical INT,
  _freq_max NUMERIC,
  _freq_min NUMERIC,
  _freq_current NUMERIC,
  _load_average NUMERIC,
  _cpu_percent NUMERIC
)
RETURNS TABLE (id int) AS $$
  BEGIN
    INSERT INTO cpu (server_id, logical, pyshical, freq_max, freq_min, freq_current, load_average, cpu_percent) VALUES (_server_id, _logical, _pyshical, _freq_max, _freq_min, _freq_current, _load_average, _cpu_percent);
    RETURN QUERY SELECT c.id FROM cpu c WHERE c.server_id = _server_id ORDER BY c.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION edit_cpu(
  _id INT,
  _server_id INT,
  _logical INT,
  _pyshical INT,
  _freq_max NUMERIC,
  _freq_min NUMERIC,
  _freq_current NUMERIC,
  _load_average NUMERIC,
  _cpu_percent NUMERIC
)
RETURNS TABLE (id INT) AS $$
  BEGIN
    UPDATE cpu c SET server_id = _server_id, logical = _logical, pyshical = _pyshical, freq_max = _freq_max, freq_min = _freq_min, freq_current = _freq_current, edit_date = CURRENT_TIMESTAMP WHERE c.id = _id;
    RETURN QUERY SELECT c.id FROM cpu c WHERE c.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;
