\c asi_tp

CREATE OR REPLACE FUNCTION get_regexs()
RETURNS TABLE (id INT, server_id INT, name VARCHAR(200), type VARCHAR(200), regex VARCHAR(200), file VARCHAR(200)) AS $$
  BEGIN
    RETURN QUERY SELECT r.id, r.server_id, r.name, r.type, r.regex, r.file FROM regex r;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_regex(
  _id INT
)
RETURNS TABLE (id INT, server_id INT, name VARCHAR(200), type VARCHAR(200), regex VARCHAR(200), file VARCHAR(200)) AS $$
  BEGIN
    RETURN QUERY SELECT r.id, r.server_id, r.name, r.type, r.regex, r.file FROM regex r WHERE r.id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_regex_server(
  _server_id INT
)
RETURNS TABLE (id INT, name VARCHAR(200), type VARCHAR(200), file VARCHAR(200), regex VARCHAR(200)) AS $$
  BEGIN
    RETURN QUERY SELECT r.id, r.name, r.type, r.file, r.regex FROM regex r WHERE r.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_regex_values(
  _regex_id INT
)
RETURNS TABLE (id INT, result TEXT, add_date TIMESTAMP) AS $$
  BEGIN
    RETURN QUERY SELECT rv.id, rv.result, rv.add_date FROM regex_values rv WHERE rv.regex_id = _regex_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_regex(
  _server_id INT,
  _name VARCHAR(200),
  _type VARCHAR(200),
  _file VARCHAR(200),
  _regex VARCHAR(200)
)
RETURNS TABLE (id int) AS $$
  DECLARE
    _val INT;
  BEGIN
    INSERT INTO regex (server_id, name, type, regex, file) VALUES (_server_id, _name, _type, _regex, _file);
    RETURN QUERY SELECT r.id FROM regex r WHERE r.server_id = _server_id ORDER BY r.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_regex_values(
  _regex_id INT,
  _result TEXT
)
RETURNS TABLE (id int) AS $$
  BEGIN
    INSERT INTO regex_values (regex_id, result) VALUES (_regex_id, _result);
    RETURN QUERY SELECT rv.id FROM regex_values rv WHERE rv.regex_id = _regex_id ORDER BY rv.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION delete_regex(
  _id INT
)
RETURNS INT AS $$
  BEGIN
    DELETE FROM regex_values rv WHERE rv.regex_id = _id;
    DELETE FROM regex r WHERE r.id = _id;
    RETURN _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION edit_regex(
  _id INT,
  _server_id INT,
  _name VARCHAR(200),
  _type VARCHAR(200),
  _file VARCHAR(200),
  _regex VARCHAR(200)
)
RETURNS TABLE (id INT) AS $$
  BEGIN
    UPDATE regex r SET server_id = _server_id, name = _name, type = _type, file = _file, regex = _regex, edit_date = CURRENT_TIMESTAMP WHERE r.id = _id;
    RETURN QUERY SELECT r.id FROM regex r WHERE r.id = _id;
  END;
$$ LANGUAGE plpgsql;
