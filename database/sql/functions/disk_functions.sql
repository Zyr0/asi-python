\c asi_tp

CREATE OR REPLACE FUNCTION get_disks()
RETURNS SETOF RECORD AS $$
  BEGIN
    RETURN QUERY SELECT * FROM disk;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_disk(
  _server_id INT,
  _mountpoint VARCHAR(200)
)
RETURNS TABLE (id INT, server_id INT, mountpoint VARCHAR(200), disabled BOOLEAN) AS $$
  BEGIN
    RETURN QUERY SELECT d.id, d.server_id, d.mountpoint, d.disabled FROM disk d WHERE d.server_id = _server_id AND d.mountpoint = _mountpoint;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_disk_server(
  _server_id INT
)
RETURNS TABLE (id INT, mountpoint VARCHAR(200)) AS $$
  BEGIN
    RETURN QUERY SELECT d.id, d.mountpoint FROM disk d WHERE d.server_id = _server_id AND d.disabled = FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_disk_values(
  _id INT
)
RETURNS TABLE (
  id INT,
  total NUMERIC,
  free NUMERIC,
  used NUMERIC,
  inodes NUMERIC,
  add_date TIMESTAMP ) AS $$
  BEGIN
    RETURN QUERY SELECT dv.id, dv.total, dv.free, dv.used, dv.inodes, dv.add_date FROM disk_values dv WHERE dv.disk_id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_disk_server_disabled(
  _server_id INT
)
RETURNS TABLE (id INT, mountpoint VARCHAR(200)) AS $$
  BEGIN
    RETURN QUERY SELECT d.id, d.mountpoint FROM disk d WHERE d.server_id = _server_id AND d.disabled = TRUE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_disk(
  _server_id INT,
  _mountpoint VARCHAR(200)
)
RETURNS TABLE (id int) AS $$
  DECLARE
    _val INT;
  BEGIN
    SELECT d.id FROM disk d INTO _val WHERE d.mountpoint = _mountpoint;
    IF _val > 0 THEN
      UPDATE disk d SET disabled = FALSE WHERE d.id = _val;
    ELSE
      INSERT INTO disk (server_id, mountpoint, disabled) VALUES (_server_id, _mountpoint, FALSE);
    END IF;
    RETURN QUERY SELECT d.id FROM disk d WHERE d.server_id = _server_id ORDER BY d.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_disk_values(
  _disk_id INT,
  _total NUMERIC,
  _free NUMERIC,
  _used NUMERIC,
  _inodes NUMERIC
)
RETURNS TABLE (id int)  AS $$
  BEGIN
    INSERT INTO disk_values (disk_id, total, free, used, inodes) VALUES (_disk_id, _total, _free, _used, _inodes);
    RETURN QUERY SELECT dv.id FROM disk_values dv WHERE dv.disk_id = _disk_id ORDER BY dv.id DESC;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION edit_disk(
  _id INT,
  _server_id INT,
  _mountpoint VARCHAR(200)
)
RETURNS INT AS $$
  BEGIN
    UPDATE disk d SET server_id = _server_id, mountpoint = _mountpoint, edit_date = CURRENT_TIMESTAMP WHERE d.id = _id;
    SELECT d.id FROM disk d WHERE d.id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disable_disk(
  _server_id INT,
  _mountpoint VARCHAR(200)
)
RETURNS TABLE (id INT) AS $$
  BEGIN
    UPDATE disk d SET disabled = TRUE WHERE d.server_id = _server_id AND d.mountpoint = _mountpoint;
    RETURN QUERY SELECT d.id FROM disk d WHERE d.server_id = _server_id AND d.mountpoint = _mountpoint;
  END;
$$ LANGUAGE plpgsql;
