\c asi_tp

CREATE OR REPLACE FUNCTION get_servers()
RETURNS TABLE (id INT, ip VARCHAR(15), name VARCHAR(255), update_time INT) AS $$
  BEGIN
    RETURN QUERY SELECT s.id, s.ip, s.name, s.update_time FROM server s;
 END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_server(
  _id INT
)
RETURNS TABLE (id INT, ip VARCHAR(15), name VARCHAR(255), update_time INT) AS $$
  BEGIN
    RETURN QUERY SELECT s.id, s.ip, s.name, s.update_time FROM server s WHERE s.id = _id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_server_values(
  _ip VARCHAR(15)
)
RETURNS TABLE (id INT, ip VARCHAR(15), v_id INT, hash TEXT) AS $$
  BEGIN
    RETURN QUERY SELECT s.id, s.ip, sv.id, sv.hash FROM server s INNER JOIN server_values sv ON s.id = sv.server_id WHERE s.ip = _ip;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_unregisted_servers()
RETURNS TABLE (id INT, ip VARCHAR(15)) AS $$
  BEGIN
    RETURN QUERY SELECT s.id, s.ip FROM server s LEFT JOIN server_values sv ON s.id = sv.server_id WHERE sv.id IS NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION get_registed_servers()
RETURNS TABLE (id INT, ip VARCHAR(15)) AS $$
  BEGIN
    RETURN QUERY SELECT s.id, s.ip FROM server s INNER JOIN server_values sv ON s.id = sv.server_id WHERE sv.id IS NOT NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_server(
  _ip VARCHAR(15),
  _name VARCHAR(255),
  _update_time INT
)
RETURNS TABLE (id int) AS $$
  BEGIN
    INSERT INTO server (ip, name, update_time) VALUES (_ip, _name, _update_time);
    RETURN QUERY SELECT s.id FROM server s WHERE s.ip = _ip AND s.name = _name;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_server_values(
  _server_id INT,
  _hash TEXT
)
RETURNS TABLE (id int) LANGUAGE SQL
AS $$
  INSERT INTO server_values (server_id, hash) VALUES (_server_id, _hash);
  SELECT id FROM server_values WHERE server_id = _server_id;
$$;

CREATE OR REPLACE FUNCTION edit_server(
  _id INT,
  _ip VARCHAR(15),
  _name VARCHAR(255),
  _update_time INT
)
RETURNS TABLE (id INT) LANGUAGE SQL
AS $$
  UPDATE server SET ip = _ip, name = _name, update_time = _update_time, edit_date = CURRENT_TIMESTAMP WHERE id = _id;
  SELECT id FROM server WHERE id = _id;
$$;

CREATE OR REPLACE FUNCTION delete_server(
  _server_id INT
)
RETURNS INT AS $$
  DECLARE
    _helper INT;
  BEGIN
    DELETE FROM regex_values rv WHERE rv.regex_id IN (SELECT r.id FROM regex r WHERE server_id = _server_id);
    DELETE FROM disk_values dv WHERE dv.disk_id IN (SELECT d.id FROM disk d WHERE server_id = _server_id);
    DELETE FROM net_values nv WHERE nv.net_id IN (SELECT n.id FROM net n WHERE server_id = _server_id);
    DELETE FROM config cn WHERE cn.server_id = _server_id;
    DELETE FROM regex r WHERE r.server_id = _server_id;
    DELETE FROM disk d WHERE d.server_id = _server_id;
    DELETE FROM net n WHERE n.server_id = _server_id;
    DELETE FROM cpu c WHERE c.server_id = _server_id;
    DELETE FROM mem_swap ms WHERE ms.server_id = _server_id;
    DELETE FROM mem_sys msys WHERE msys.server_id = _server_id;
    DELETE FROM server_values sv WHERE sv.server_id = _server_id;
    DELETE FROM server s WHERE s.id = _server_id;
    RETURN 1;
  END;
$$ LANGUAGE plpgsql;
