\c asi_tp

CREATE OR REPLACE FUNCTION get_configs()
RETURNS SETOF RECORD LANGUAGE SQL
AS $$
  SELECT * FROM config;
$$;

CREATE OR REPLACE FUNCTION get_config(
  _id INT
)
RETURNS RECORD LANGUAGE SQL
AS $$
  SELECT * FROM config WHERE id = _id;
$$;

CREATE OR REPLACE FUNCTION get_config_server(
  _server_id INT
)
RETURNS TABLE (id INT, cpu BOOLEAN, disk BOOLEAN,
  mem_sys BOOLEAN, mem_swap BOOLEAN, net BOOLEAN) AS $$
  BEGIN
    RETURN QUERY SELECT c.id, c.cpu, c.disk, c.mem_sys, c.mem_swap, c.net FROM config c WHERE c.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_config(
  _server_id INT,
  _cpu BOOLEAN,
  _disk BOOLEAN,
  _mem_sys BOOLEAN,
  _mem_swap BOOLEAN,
  _net BOOLEAN
)
RETURNS TABLE (id int) LANGUAGE SQL
AS $$
  INSERT INTO config (server_id, cpu, disk, mem_sys, mem_swap, net) VALUES (_server_id, _cpu, _disk, _mem_sys, _mem_swap, _net);
  SELECT id FROM config WHERE server_id = _server_id;
$$;

CREATE OR REPLACE FUNCTION edit_config(
  _server_id INT,
  _cpu BOOLEAN,
  _disk BOOLEAN,
  _mem_sys BOOLEAN,
  _mem_swap BOOLEAN,
  _net BOOLEAN
)
RETURNS TABLE (id INT) LANGUAGE SQL
AS $$
  UPDATE config SET cpu = _cpu, disk = _disk, mem_sys = _mem_sys, mem_swap = _mem_swap, net = _net, edit_date = CURRENT_TIMESTAMP WHERE server_id = _server_id;
  SELECT id FROM config WHERE server_id = _server_id;
$$;
