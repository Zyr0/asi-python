\c asi_tp

CREATE OR REPLACE FUNCTION get_mem_sys()
RETURNS SETOF RECORD LANGUAGE SQL
AS $$
  SELECT * FROM mem_sys;
$$;

CREATE OR REPLACE FUNCTION get_mem_sys(
  _id INT
)
RETURNS RECORD LANGUAGE SQL
AS $$
  SELECT * FROM mem_sys WHERE id = _id;
$$;

CREATE OR REPLACE FUNCTION get_mem_sys_server(
  _server_id INT
)
RETURNS TABLE (
  id INT,
  server_id INT,
  perc NUMERIC,
  total NUMERIC,
  available NUMERIC,
  used NUMERIC,
  free NUMERIC,
  add_date TIMESTAMP) AS $$
  BEGIN
    RETURN QUERY SELECT ms.id, ms.server_id, ms.perc, ms.total, ms.available, ms.used, ms.free, ms.add_date FROM mem_sys ms WHERE ms.server_id = _server_id;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION add_mem_sys(
  _server_id INT,
  _perc NUMERIC,
  _total NUMERIC,
  _available NUMERIC,
  _free NUMERIC,
  _used NUMERIC
)
RETURNS TABLE (id int) LANGUAGE SQL
AS $$
  INSERT INTO mem_sys (server_id, perc, total, available, free, used) VALUES (_server_id, _perc, _total, _available, _free, _used);
  SELECT id FROM mem_sys WHERE server_id = _server_id ORDER BY id DESC;
$$;

CREATE OR REPLACE FUNCTION edit_mem_sys(
  _id INT,
  _server_id INT,
  _perc NUMERIC,
  _total NUMERIC,
  _available NUMERIC,
  _free NUMERIC,
  _used NUMERIC
)
RETURNS RECORD LANGUAGE SQL
AS $$
  UPDATE mem_sys SET server_id = _server_id, perc = _perc, total = _total, available = _available, free = _free, used = _used, edit_date = CURRENT_TIMESTAMP WHERE id = _id;
  SELECT * FROM mem_sys WHERE server_id = _server_id;
$$;
