#!/bin/sh
user=''
dir='.'

show_usage() {
  printf "Usage: $0 [options]\n"
  printf "\n"
  printf "Options:\n"
  printf "  -t|--table. Run table.sql files\n"
  printf "  -f|--func. Run functions files\n"
  printf "  -tf|--table --func. Run table.sql and functions files\n"
  printf "  -h|--help. Print help\n\n"
}

get_creds() {
  read -p "Username: " user
}

run_tables() {
  [ -z $user ] && get_creds
  echo Running tables.sql ...
  psql -U $user -f $dir/tables.sql
}

run_functions() {
  [ -z $user ] && get_creds
  for file in $dir/functions/*
  do
    echo 'Running file' $file '...'
    psql -U $user -f $file
  done
}

run_all() {
  run_tables
  run_functions
}

case "$1" in
    -d)
      dir='/sql'
      user='asi'
      run_all
      ;;
    -tf|-ft)
      run_all
      ;;
    -t|--table)
      run_tables
      ;;
    -f|--func)
      run_functions
      ;;
    *)
      show_usage
      ;;
esac
